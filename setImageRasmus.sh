microk8s.kubectl set image deployment/frontend frontend=registry.gitlab.com/swp7/p7-project/frontend:rasmus
microk8s.kubectl set image deployment/backend backend=registry.gitlab.com/swp7/p7-project/backend:rasmus
microk8s.kubectl rollout restart deployment/backend
microk8s.kubectl rollout restart deployment/frontend
