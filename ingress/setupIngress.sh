kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.41.2/deploy/static/provider/baremetal/deploy.yaml

echo "Assuming server ip is still 130.225.57.128"
kubectl patch svc -n ingress-nginx ingress-nginx-controller -p '{"spec":{"externalIPs":["130.225.57.128"]}}'
kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/v0.16.1/cert-manager.yaml
kubectl create -f certprod.yaml
kubectl create -f ingress.yaml
