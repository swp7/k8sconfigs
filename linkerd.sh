sudo snap alias microk8s.kubectl kubectl
mkdir $HOME/.kube
microk8s config > $HOME/.kube/config
curl -sL https://run.linkerd.io/install | sh
export PATH=$PATH:$HOME/.linkerd2/bin
linkerd version
linkerd check --pre

linkerd install | kubectl apply -f -
linkerd check
microk8s.kubectl -n linkerd get deploy

